/*	stack.c: Stack application. */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "dynArray.h"

# define TYPE      char


/* ***************************************************************
Using stack to check for unbalanced parentheses.
***************************************************************** */

/* Returns the next character of the string, once reaches end return '0' (zero)
	param: 	s pointer to a string 	
			
*/
char nextChar(char *s) {
    static int i = -1;
    char c;
    ++i;
    c = *(s + i);
    if (c == '\0')
        return '\0';
    else
        return c;
}

/* Checks whether the (), {}, and [] are balanced or not
	param: 	s pointer to a string 	
	pre: 	
	post:	
*/
int isBalanced(char *s) {
    /* FIXME: You will write this function */

    DynArr *balanceStack;
    balanceStack = newDynArr(5);

    int complete = 0;

    while (complete == 0) {
        char c = nextChar(s);
        if (c == '\0') {
            complete = 1;
        } else {
            if (c == '(' || c == '{' || c == '[') {
                pushDynArr(balanceStack, c);
            } else if (c == ')' && topDynArr(balanceStack) == '(') {
                popDynArr(balanceStack);
            } else if (c == '}' && topDynArr(balanceStack) == '{') {
                popDynArr(balanceStack);
            } else if (c == ']' && topDynArr(balanceStack) == '[') {
                popDynArr(balanceStack);
            }
        }
    }


    if (isEmptyDynArr(balanceStack)) {
        deleteDynArr(balanceStack);
        return 1;
    }

    deleteDynArr(balanceStack);
    return 0;
}

int main(int argc, char *argv[]) {

    char *s = argv[1];
    int res;

    printf("Assignment 2\n");

    res = isBalanced(s);

    if (res)
        printf("The string %s is balanced\n", s);
    else
        printf("The string %s is not balanced\n", s);
    
    return 0;
}

