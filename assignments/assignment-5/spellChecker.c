#include "hashMap.h"
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct Suggestion {
    char *string;
    int levDistance;
};

struct Suggs {
    struct Suggestion *suggestion[6];
    int suggestion_count;
};

void addSuggestion(struct Suggs *suggestions, char* string, int levDistance) {
    struct Suggestion* suggestion = malloc(sizeof(struct Suggestion));
    suggestion->string = string;
    suggestion->levDistance = levDistance;

    if (suggestions->suggestion_count == 0) {
        suggestions->suggestion[0] = suggestion;
        suggestions->suggestion_count++;
    } else {
        for (int x = 0; x < suggestions->suggestion_count; x++){
            if (suggestion->levDistance < suggestions->suggestion[x]->levDistance) {
                    struct Suggestion *temp = suggestions->suggestion[x];
                    suggestions->suggestion[x] = suggestion;
                    suggestion = temp;
            }
        }

        if (suggestions->suggestion_count < 6){
            suggestions->suggestion[suggestions->suggestion_count] = suggestion;
            suggestions->suggestion_count++;

        } else {
            free(suggestion);
        }
    }
}

void printSuggestions(struct Suggs *suggestions) {
    for (int x = 0; x < 6; x++) {
        printf("%s \n", suggestions->suggestion[x]->string);
    }
}

void clearSuggestions(struct Suggs *suggestions){
    for(int i = 0; i<suggestions->suggestion_count; i++){
        free(suggestions->suggestion[i]);
    }
}

int getMin(int a, int b, int c) {
    if (a < b && a < c) {
        return a;
    } else if (b < c) {
        return b;
    } else {
        return c;
    }
}

int LevenshteinDistance(char *string1, char *string2) {
    int rows = strlen(string1) + 1;
    int columns = strlen(string2) + 1;
    int matrix[rows][columns];

    for (int x = 0; x < rows; x++) {
        for (int y = 0; y < columns; y++) {
            matrix[x][y] = 0;
        }
    }

    for (int x = 0; x < rows; x++) {
        matrix[x][0] = x;
    }

    for (int y = 0; y < columns; y++) {
        matrix[0][y] = y;
    }

    for (int x = 1; x < rows; x++) {
        for (int y = 1; y < columns; y++) {
            int substitutionCost;
            if (string1[x] == string2[y]) {
                substitutionCost = 0;
            } else {
                substitutionCost = 1;
            }
            matrix[x][y] = getMin(
                    matrix[(x - 1)][y] + 1,                   // deletion
                    matrix[x][(y - 1)] + 1,                   // insertion
                    matrix[(x - 1)][(y - 1)] + substitutionCost);  // substitution
        }
    }

    int score = (matrix[rows - 1][columns - 1]);

    return score;
}


/**
 * Allocates a string for the next word in the file and returns it. This string
 * is null terminated. Returns NULL after reaching the end of the file.
 * @param file
 * @return Allocated string or NULL.
 */
char *nextWord(FILE *file) {
    int maxLength = 16;
    int length = 0;
    char *word = malloc(sizeof(char) * maxLength);
    while (1) {
        char c = fgetc(file);
        if ((c >= '0' && c <= '9') ||
            (c >= 'A' && c <= 'Z') ||
            (c >= 'a' && c <= 'z') ||
            c == '\'') {
            if (length + 1 >= maxLength) {
                maxLength *= 2;
                word = realloc(word, maxLength);
            }
            word[length] = c;
            length++;
        } else if (length > 0 || c == EOF) {
            break;
        }
    }
    if (length == 0) {
        free(word);
        return NULL;
    }
    word[length] = '\0';
    return word;
}

/**
 * Loads the contents of the file into the hash map.
 * @param file
 * @param map
 */
void loadDictionary(FILE *file, HashMap *map) {
    // FIXME: implement
    char *word = nextWord(file);

    while (word) {
        hashMapPut(map, word, 1);
        free(word);
        word = nextWord(file);
    }

    free(word);
}


/**
 * Prints the concordance of the given file and performance information. Uses
 * the file input1.txt by default or a file name specified as a command line
 * argument.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, const char **argv) {
    // FIXME: implement
    HashMap *map = hashMapNew(1000);

    FILE *file = fopen("dictionary.txt", "r");
    clock_t timer = clock();
    loadDictionary(file, map);
    timer = clock() - timer;
    //Printing Performance Information
    printf("Dictionary loaded in %f seconds\n",
           (float) timer / (float) CLOCKS_PER_SEC);
    fclose(file);

    //Printing Concordance of given file (mentioned in docstring above.
    //hashMapPrint(map);
    char inputBuffer[256];
    int quit = 0;

    while (!quit) {
        printf("Enter a word or \"quit\" to quit: ");
        scanf("%s", inputBuffer);

        // Implement the spell checker code here..

        if (strcmp(inputBuffer, "quit") == 0) {
            quit = 1;
        } else {
            if (hashMapContainsKey(map, inputBuffer)) {
                printf("%s is spelled correctly.\n", inputBuffer);
            } else {
                struct Suggs *suggestions = malloc(sizeof(struct Suggs));
                suggestions->suggestion_count = 0;

                //start with the words that could be the closes
                for (int i = 0; i < hashMapCapacity(map); i++) {
                    HashLink *current = map->table[i];
                    while (current != 0) {
                        addSuggestion(suggestions,
                                current->key,
                                LevenshteinDistance(inputBuffer, current->key));
                        current = current->next;
                    }
                }

                /* Print suggestions */
                printf("Perhaps you meant...\n");
                printSuggestions(suggestions);

                clearSuggestions(suggestions);
                free(suggestions);
            }
        }
    }

    hashMapDelete(map);
    return 0;
}