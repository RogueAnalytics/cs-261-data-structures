/* CS261- Assignment 1 - Q.0 */
/* Name: W. Aaron Morris
 * Date: 1/10/2018
 * Solution description: Simple reassignment of value by de-referencing the pointer and address by reassignment.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void fooA(int *iptr) {

    /*Print the value and address of the integer pointed to by iptr*/
    printf("fooA iptr: %i\n", *iptr);

    /*Increment the value of the integer pointed to by iptr by 5*/
    *iptr = *iptr + 1;

    /*Print the address of iptr itself*/
    printf("fooA &iptr: %p\n", &iptr);

    /*Dummy variable, ignore the warning*/
    int c = 5;
}


void fooB(int *jptr) {

    /*Print the value and address of the integer pointed to by jptr*/
    printf("fooB *jpter: %i\n", *jptr);
    printf("fooB jptr: %p\n", jptr);

    /*Decrement the address by 1 pointed to by jptr using jptr */
    jptr = jptr - 1;

    /*Print the address of jptr itself*/
    printf("fooB jptr: %p\n", jptr);
}


int main() {

    /*Declare an integer x and initialize it randomly to a value in [0,10] */
    srand(time(NULL));
    int x = rand() % 10;

    /*Print the value and address of x*/
    printf("main x: %i\n", x);
    printf("main &x: %p\n", &x);

    /*Call fooA() with the address of x*/
    fooA(&x);

    /*Print the value of x*/
    printf("main x: %i\n", x);

    /*
     * The value of x changes because the address of x is passed into fooA();
     * fooA() dereferences the address and sets the value of x.
     */

    /*Call fooB() with the address of x*/
    fooB(&x);

    /*Print the value and address of x*/
    printf("main x: %i\n", x);
    printf("main &x: %p\n", &x);

    /*
     * The value and address of x does not change because of scoping. When fooB
     * is called, the pointer jptr is place on the stack with the address equal
     * to that of x. When the address of jptr is modified, the program does not
     * change the address of x because jptr is an object that references the
     * location of x. In order to change the address-and subsequently the value-
     * of x, jptr would need to be a pointer to a pointer that could be
     * de-referenced and changed.
     */

    return 0;
}

