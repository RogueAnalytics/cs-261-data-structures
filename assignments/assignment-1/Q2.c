/* CS261- Assignment 1 - Q.2*/
/* Name: W. Aaron Morris
 * Date: 1/11/2019
 * Solution description: Simply Swap the Address of the pointer in foo.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int foo(int* a, int* b, int c){
    /*Swap the addresses stored in the pointer variables a and b*/
    int *temp;
    printf("%p\n", a);
    printf("%p\n", b);
    temp = a;
    printf("%p\n", temp);
    a = b;
    printf("%p\n", a);
    printf("%p\n", b);
    b = temp;
    printf("%p\n", a);
    printf("%p\n", b);
    printf("%p\n", temp);

    /*Decrement the value of integer variable c*/
    --c;

    /*Return c*/
    return c;
}

int main(){
    /*Declare three integers x,y and z and initialize them randomly to values in [0,10] */
    srand(time(NULL));

    int x = rand() % 11;
    int y = rand() % 11;
    int z = rand() % 11;
    
    /*Print the values of x, y and z*/
    printf("x : %i \n", x);
    printf("y : %i \n", y);
    printf("z : %i \n", z);
    
    /*Call foo() appropriately, passing x,y,z as parameters*/
    int a = foo(&x, &y, z);
    
    /*Print the values of x, y and z*/
    printf("x : %i \n", x);
    printf("y : %i \n", y);
    printf("z : %i \n", z);

    
    /*Print the value returned by foo*/
    printf("a : %i \n", a);
    
    return 0;
}
    
/*
 * a) Is the return value different than the value of integer z? Why or why not?
 *
 * Yes. In the function foo, we return the value after decrementing the value by
 * one.
 *
 *******************************************************************************
 *
 * b) Are the values of integers x and y different before and after calling the
 *    function foo(..)? Why or why not?
 *
 * No. The reason the values are not different deals with scoping. When the
 * address of x and y are passed into foo, they are copied onto the stack into
 * their own pointers. As the address of these pointers change, they do not
 * change the original address of the variable x and y that are in the scope of
 * the main function.
 *
 */


