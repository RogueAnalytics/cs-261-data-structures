/* CS261- Assignment 1 - Q.3*/
/* Name: W. Aaron Morris
 * Date: 1/11/2019
 * Solution description: convert to proper underscore and subsquently make camelCase.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


char toUpperCase(char ch) {
    /*Convert ch to upper case, assuming it is in lower case currently*/
    return toupper(ch);
}

char toLowerCase(char ch) {
    /*Convert ch to lower case, assuming it is in upper case currently*/
    return tolower(ch);
}

int stringLength(char s[]) {
    /*Return the length of the string*/
    int count = 0;
    while (s[count] != '\0') {
        count++;
    }

    return count;
}


void camelCase(char *word) {
    /*Convert to camelCase*/
    int nextCapital = 0;
    int firstLetter = 1;
    int letterCount = 0;
    for (int i = 0; i < stringLength(word); i++) {
        if (isalpha(word[i])) {
            if (nextCapital == 1 && firstLetter == 0) {
                word[letterCount] = toUpperCase(word[i]);
                nextCapital = 0;
            } else {
                word[letterCount] = toLowerCase(word[i]);
                firstLetter = 0;
            }
            letterCount++;
        } else {
            nextCapital = 1;
        }
    }
    for (int i = letterCount; i < stringLength(word); i++) {
        word[i] = '\0';
    }
}

int main() {
    int validString = 0;
    char word[256] = "";

    /*Read the string from the keyboard*/
    printf("Insert String:\n");
    scanf("%[^\n]", word);

    int validAlpha = 0;
    int lastUnder = 0;
    int letterCount = 0;


    for (int i = 0; i < stringLength(word); i++) {
        if ((word[i] >= 'A' && word[i] <= 'Z') ||
            (word[i] >= 'a' && word[i] <= 'z')) {
            word[letterCount] = toLowerCase(word[i]);
            if (validAlpha == 1 && lastUnder == 1) {
                validString = 1;
            }
            lastUnder = 0;
            validAlpha = 1;
            ++letterCount;
        } else if ((word[i] < 'A' || word[i] > 'Z') &&
                   (word[i] < 'a' || word[i] > 'z')) {
            if (lastUnder == 0 && validAlpha == 1) {
                word[letterCount] = '_';
                lastUnder = 1;
                ++letterCount;
            }
        }
    }

    for (int i = letterCount; i < stringLength(word); i++) {
        word[i] = '\0';
    }

    /*Call camelCase*/
    if (validString == 1) {
        validString = 1;
        camelCase(word);
    } else {
        printf("Invalid String\n");
    }

    /*Print the new string*/
    printf("%s\n", word);

    return 0;
}

