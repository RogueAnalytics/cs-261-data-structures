/* CS261- Assignment 1 - Q.1*/
/* Name: W. Aaron Morris
 * Date: 1/11/2019
 * Solution description: After allocating memory for the struct, loop through students to ensure id is not already taken.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

struct student {
    int id;
    int score;
};

struct student *allocate() {
    /* Allocate memory for ten students */
    struct student *students;
    students = (struct student *) malloc(10 * sizeof(struct student));

    /* Return the pointer */
    return students;
}

void generate(struct student *students) {
    /* Generate random and unique IDs and random scores for ten students,
     * IDs being between 1 and 10, scores between 0 and 100 */
    srand(time(NULL));

    for (int i = 0; i < 10; i++) {
        int id;
        int validID = 0;
        do {
            id = rand() % 10 + 1;
            for (int j = i; j >= 0; j--){
                if (students[j].id == id){
                    validID = 0;
                    j = -1;
                } else {
                    validID = 1;
                }
            }
        } while (validID == 0);


        students[i].id = id;
        students[i].score = rand() % 101;
    }
}

void output(struct student *students) {
    /*Output information about the ten students in the format:
             ID1 Score1
             ID2 score2
             ID3 score3
             ...
             ID10 score10*/
    for (int i = 0; i < 10; i++) {
        printf("%i ", students[i].id);
        printf("%i\n", students[i].score);
    }
}

void summary(struct student *students) {
    /* Compute and print the minimum, maximum and average scores of the
     * ten students
     */
    int min = 100;
    int max = 0;
    double average = 0;

    for (int i = 0; i < 10; i++) {
        if (students[i].score < min) {
            min = students[i].score;
        }

        if (students[i].score > max) {
            max = students[i].score;
        }

        average += students[i].score;
    }

    average = average / 10;

    printf("Min: %i\n", min);
    printf("Max: %i\n", max);
    printf("Average: %f\n", average);


}

void deallocate(struct student *stud) {
    /*Deallocate memory from stud*/
    if (stud==0){
        free(stud);
    }
}

int main() {
    struct student *st = 0;

    /*Call allocate*/
    st = allocate();

    /*Call generate*/
    generate(st);

    /*Call output*/
    output(st);

    /*Call summary*/
    summary(st);

    /*Call deallocate*/
    deallocate(st);

    return 0;
}

